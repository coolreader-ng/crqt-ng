<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../aboutdlg.ui" line="20"/>
        <source>About crqt-ng</source>
        <translation>Over crqt-ng</translation>
    </message>
    <message>
        <location filename="../aboutdlg.ui" line="58"/>
        <source>About</source>
        <translation>Over</translation>
    </message>
    <message>
        <location filename="../aboutdlg.ui" line="79"/>
        <source>License</source>
        <translation>Licentie</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="44"/>
        <source>crqt-ng is free open source e-book viewer based on crengine-ng library.</source>
        <translation>crqt-ng is een gratis, open source e-booklezer, gemaakt met de crengine-ng-bibliotheek.</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="45"/>
        <source>Source code is available at</source>
        <translation>De broncode is beschikbaar op</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="46"/>
        <source>under the terms of GNU GPL license either version 2 or (at your option) any later version.</source>
        <translation>onder de voorwaarden van de GNU GPL-licentie, versie 2 of (naar eigen inzicht) hoger.</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="48"/>
        <source>It is a fork of the &apos;CoolReader&apos; program.</source>
        <translation>Het programma is afgesplitst van ‘CoolReader’.</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="50"/>
        <source>Third party components used in crengine-ng:</source>
        <translation>Externe onderdelen in crengine-ng:</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="53"/>
        <source>FreeType - font rendering</source>
        <translation>FreeType - lettertyperendering</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="56"/>
        <source>HarfBuzz - text shaping, font kerning, ligatures</source>
        <translation>HarfBuzz - tekstvorming, lettertypekernening, ligaturen</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="59"/>
        <source>ZLib - compressing library</source>
        <translation>ZLib - compressiebibliotheek</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="62"/>
        <source>ZSTD - compressing library</source>
        <translation>ZSTD - compressiebibliotheek</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="65"/>
        <source>libpng - PNG image format support</source>
        <translation>libpng - png-afbeeldingsondersteuning</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="68"/>
        <source>libjpeg - JPEG image format support</source>
        <translation>libjpeg - jpeg-afbeeldingsondersteuning</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="71"/>
        <source>FriBiDi - RTL writing support</source>
        <translation>FriBiDi - van-links-naar-rechts-taalondersteuning</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="74"/>
        <source>libunibreak - line breaking and word breaking algorithms</source>
        <translation>libunibreak - regel- en woordafbrekingsalgoritmen</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="77"/>
        <source>utf8proc - for unicode string comparision</source>
        <translation>utf8proc - voor unicode-tekenreeksvergelijkingen</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="80"/>
        <source>NanoSVG - SVG image format support</source>
        <translation>NanoSVG - svg-afbeeldingsondersteuning</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="83"/>
        <source>chmlib - chm format support</source>
        <translation>chmlib - chm-ondersteuning</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="86"/>
        <source>antiword - Microsoft Word format support</source>
        <translation>antiword - Microsoft Word-ondersteuning</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="89"/>
        <source>RFC6234 (sources) - SHAsum</source>
        <translation>RFC6234 (bronnen) - SHAsum</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="92"/>
        <source>cmark - CommonMark parsing and rendering library and program in C</source>
        <translation>cmark - CommonMark-verwerking, -renderingbibliotheek en -programma, geschreven in C</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="96"/>
        <source>cmark-gfm - GitHub&apos;s fork of cmark, a CommonMark parsing and rendering library and program in C</source>
        <translation>cmark - GitHubs afsplitsing van cmark, een CommonMark-verwerking, -renderingbibliotheek en -programma, geschreven in C</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="101"/>
        <source>MD4C - MD4C stands for &quot;Markdown for C&quot; and that&apos;s exactly what this project is about.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="104"/>
        <source>hyphman - AlReader hyphenation manager</source>
        <translation>hyphman - AlReader-woordafbrekingsbeheer</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="105"/>
        <source>Most hyphenation dictionaries - TEX hyphenation patterns</source>
        <translation>De meeste woordafbrekingswoordenboeken - TEX-woordafbrekingspatronen</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="106"/>
        <source>Russian hyphenation dictionary - </source>
        <translation>Russisch woordafbrekingswoordenboek - </translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="109"/>
        <source>Languages character set database by Fontconfig</source>
        <translation>Tekencoderingsdatabank van Fontconfig</translation>
    </message>
</context>
<context>
    <name>AddBookmarkDialog</name>
    <message>
        <location filename="../addbookmarkdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoogvenster</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="22"/>
        <source>Bookmark type</source>
        <translation>Soort bladwijzer</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="32"/>
        <location filename="../addbookmarkdlg.cpp" line="54"/>
        <source>Position</source>
        <translation>Locatie</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="39"/>
        <source>Page 1/10 10%</source>
        <translation>Pagina 1/10 10%</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="46"/>
        <source>Position text</source>
        <translation>Locatietekst</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="53"/>
        <location filename="../addbookmarkdlg.cpp" line="56"/>
        <location filename="../addbookmarkdlg.cpp" line="122"/>
        <source>Comment</source>
        <translation>Opmerking</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="66"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="73"/>
        <source>sample title</source>
        <translation>Voorbeeldtitel</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.cpp" line="49"/>
        <source>Add bookmark</source>
        <translation>Bladwijzer toevoegen</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.cpp" line="57"/>
        <location filename="../addbookmarkdlg.cpp" line="126"/>
        <source>Correction</source>
        <translation>Correctie</translation>
    </message>
</context>
<context>
    <name>BookmarkListDialog</name>
    <message>
        <location filename="../bookmarklistdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoogvenster</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="49"/>
        <source>Go to Bookmark</source>
        <translation>Ga naar bladwijzer</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="52"/>
        <source>Return</source>
        <translation>Terug</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="57"/>
        <source>Remove Bookmark</source>
        <translation>Bladwijzer verwijderen</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="60"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="65"/>
        <source>Remove ALL Bookmarks</source>
        <translation>ALLE bladwijzers verwijderen</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="70"/>
        <source>Edit Bookmark</source>
        <translation>Bladwijzer bewerken</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="73"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="78"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="81"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="46"/>
        <source>Bookmarks</source>
        <translation>Bladwijzers</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="50"/>
        <source>Position</source>
        <translation>Locatie</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="50"/>
        <source>Type</source>
        <comment>bookmark type</comment>
        <translation>Soort</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="50"/>
        <source>Text</source>
        <translation>Tekst</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="51"/>
        <source>Comment</source>
        <translation>Opmerking</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="88"/>
        <source>P</source>
        <comment>Bookmark type first letter - Position</comment>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="90"/>
        <source>C</source>
        <comment>Bookmark type first letter - Comment</comment>
        <translation>O</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="92"/>
        <source>E</source>
        <comment>Bookmark type first letter - Correction/Edit</comment>
        <translation>B</translation>
    </message>
</context>
<context>
    <name>CR3View</name>
    <message>
        <location filename="../cr3widget.cpp" line="1202"/>
        <source>Warning</source>
        <translation>Waarschuwing</translation>
    </message>
    <message>
        <location filename="../cr3widget.cpp" line="1203"/>
        <source>Font &quot;%1&quot; isn&apos;t compatible with language &quot;%2&quot;. Instead will be used fallback font.</source>
        <translation>‘%1’ is niet compatibel met de taal ‘%2’. Er wordt een alternatief lettertype gebruikt.</translation>
    </message>
    <message>
        <location filename="../cr3widget.cpp" line="1316"/>
        <source>Error while opening document </source>
        <translation>Het bestand kan niet worden geopend </translation>
    </message>
    <message>
        <location filename="../cr3widget.cpp" line="1719"/>
        <source>Loading: please wait...</source>
        <translation>Bezig met laden…</translation>
    </message>
</context>
<context>
    <name>ExportProgressDlg</name>
    <message>
        <location filename="../exportprogressdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoogvenster</translation>
    </message>
    <message>
        <location filename="../exportprogressdlg.ui" line="20"/>
        <source>Export is in progress...</source>
        <translation>Bezig met exporteren…</translation>
    </message>
</context>
<context>
    <name>FallbackFontsDialog</name>
    <message>
        <location filename="../fallbackfontsdialog.ui" line="14"/>
        <source>Fallback fonts</source>
        <translation>Alternatieve lettertypen</translation>
    </message>
    <message>
        <location filename="../fallbackfontsdialog.ui" line="23"/>
        <source>List of fallback fonts:</source>
        <translation>Lijst met alternatieve lettertypen:</translation>
    </message>
    <message>
        <location filename="../fallbackfontsdialog.cpp" line="163"/>
        <source>Remove this fallback font</source>
        <translation>Alternatief lettertype verwijderen</translation>
    </message>
</context>
<context>
    <name>FilePropsDialog</name>
    <message>
        <location filename="../filepropsdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoogvenster</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="41"/>
        <source>Document properties</source>
        <translation>Bestandseigenschappen</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="44"/>
        <source>Property</source>
        <translation>Eigenschap</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="44"/>
        <source>Value</source>
        <translation>Waarde</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="186"/>
        <source>Current page</source>
        <translation>Huidige pagina</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="187"/>
        <source>Total pages</source>
        <translation>Totaalaantal pagina&apos;s</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="188"/>
        <source>Battery state</source>
        <translation>Accustatus</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="189"/>
        <source>Current Time</source>
        <translation>Huidig tijdstip</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="194"/>
        <source>Current chapter</source>
        <translation>Huidig hoofdstuk</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="197"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="199"/>
        <source>Archive name</source>
        <translation>Archiefnaam</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="200"/>
        <source>Archive path</source>
        <translation>Archieflocatie</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="201"/>
        <source>Archive size</source>
        <translation>Archiefomvang</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="202"/>
        <source>File name</source>
        <translation>Bestandsnaam</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="203"/>
        <source>File path</source>
        <translation>Bestandslocatie</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="204"/>
        <source>File size</source>
        <translation>Bestandsgrootte</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="205"/>
        <source>File format</source>
        <translation>Bestandsformaat</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="206"/>
        <source>File info</source>
        <translation>Bestandsinformatie</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="208"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="209"/>
        <source>Author(s)</source>
        <translation>Auteur(s)</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="210"/>
        <source>Series name</source>
        <translation>Serienaam</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="211"/>
        <source>Series number</source>
        <translation>Serienummer</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="212"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="213"/>
        <source>Genres</source>
        <translation>Genres</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="214"/>
        <source>Language</source>
        <translation>Taal</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="215"/>
        <source>Translator</source>
        <translation>Vertaler</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="216"/>
        <source>Book info</source>
        <translation>Boekinformatie</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="218"/>
        <source>Document author</source>
        <translation>Bestandsauteur</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="219"/>
        <source>Document date</source>
        <translation>Bestandsdatum</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="220"/>
        <source>Document source URL</source>
        <translation>Bestandsbron-url</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="221"/>
        <source>OCR by</source>
        <translation>OCR van</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="222"/>
        <source>Document version</source>
        <translation>Bestandsversie</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="223"/>
        <source>Document info</source>
        <translation>Bestandsinformatie</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="225"/>
        <source>Publication name</source>
        <translation>Publicatienaam</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="226"/>
        <source>Publisher</source>
        <translation>Uitgever</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="227"/>
        <source>Publisher city</source>
        <translation>Locatie van uitgever</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="228"/>
        <source>Publication year</source>
        <translation>Publicatiejaar</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="229"/>
        <source>ISBN</source>
        <translation>ISBN</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="230"/>
        <source>Publication info</source>
        <translation>Publicatie-informatie</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="232"/>
        <source>Custom info</source>
        <translation>Aanvullende informatie</translation>
    </message>
</context>
<context>
    <name>FontFamiliesDialog</name>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="23"/>
        <source>Generic font families</source>
        <translation>Algemene lettertypen</translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="36"/>
        <source>Generic font families settings</source>
        <translation>Algemene lettertype-instellingen</translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="45"/>
        <source>Serif</source>
        <translation>Met schreef</translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="62"/>
        <source>Sans-Serif</source>
        <translation>Schreefloos</translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="79"/>
        <source>Cursive</source>
        <translation>Cursief</translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="96"/>
        <source>Fantasy</source>
        <translation>Fantasie</translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="113"/>
        <source>Monospace</source>
        <translation>Vaste breedte</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="191"/>
        <location filename="../mainwindow.cpp" line="1041"/>
        <location filename="../mainwindow.cpp" line="1066"/>
        <source>Warning</source>
        <translation>Waarschuwing</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="191"/>
        <location filename="../mainwindow.cpp" line="1041"/>
        <location filename="../mainwindow.cpp" line="1066"/>
        <source>The maximum number of tabs has been exceeded!</source>
        <translation>Het maximumaantal tabbladen is bereikt!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="293"/>
        <source>Open book file</source>
        <translation>Kies een boekbestand</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="295"/>
        <source>All supported formats</source>
        <translation>Alle ondersteunde formatien</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="296"/>
        <source>FB2 books</source>
        <translation>FB2-boeken</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="297"/>
        <source>FB3 books</source>
        <translation>FB3-boeken</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="298"/>
        <source>Text files</source>
        <translation>Tekstbestanden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="299"/>
        <source>Rich text</source>
        <translation>Richtext</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="300"/>
        <source>MS Word document</source>
        <translation>MS Word-document</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="301"/>
        <source>Open Document files</source>
        <translation>OpenDocument-bestanden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="302"/>
        <source>HTML files</source>
        <translation>Html-bestanden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="304"/>
        <source>Markdown files</source>
        <translation>Markdownbestanden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="306"/>
        <source>EPUB files</source>
        <translation>Epub-bestanden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="308"/>
        <source>CHM files</source>
        <translation>Chm-bestanden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="310"/>
        <source>MOBI files</source>
        <translation>Mobi-bestanden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="311"/>
        <source>PalmDOC files</source>
        <translation>PalmDOC-bestanden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="312"/>
        <source>ZIP archives</source>
        <translation>Zipbestanden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>Export document to</source>
        <translation>Document exporteren naar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>WOL book (*.wol)</source>
        <translation>WOL-boek (*.wol)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>Export to WOL format</source>
        <translation>Exporteren naar WOL-formaat</translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>crqt-ng</source>
        <translation>crqt-ng</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="78"/>
        <source>File</source>
        <translation>Bestand</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="93"/>
        <source>View</source>
        <translation>Beeld</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="109"/>
        <source>Navigation</source>
        <translation>Navigatie</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="125"/>
        <source>Help</source>
        <translation>Hulp</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="169"/>
        <source>Open...</source>
        <translation>Openen…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="172"/>
        <source>Open file</source>
        <translation>Bestand openen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="175"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="180"/>
        <source>Toggle Pages/Scroll</source>
        <translation>Pagina-/Verschuifmodus</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="183"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="192"/>
        <source>Close</source>
        <translation>Afsluiten</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="195"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="203"/>
        <source>Minimize</source>
        <translation>Minimaliseren</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="206"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="218"/>
        <source>Page Down</source>
        <translation>Pagina omlaag</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="221"/>
        <source>Go to next page</source>
        <translation>Ga naar volgende pagina</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="224"/>
        <source>PgDown</source>
        <translation>PgDown</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="233"/>
        <source>Page Up</source>
        <translation>Pagina omhoog</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="236"/>
        <source>Back by page</source>
        <translation>Ga naar vorige pagina</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="239"/>
        <source>PgUp</source>
        <translation>PgUp</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="244"/>
        <source>Line Down</source>
        <translation>Regel omlaag</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="247"/>
        <source>Forward by one line</source>
        <translation>Regel vooruit</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="250"/>
        <source>Down</source>
        <translation>Pijltje omlaag</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="255"/>
        <source>Line Up</source>
        <translation>Regel omhoog</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="258"/>
        <source>Back by line</source>
        <translation>Regel terug</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="261"/>
        <source>Up</source>
        <translation>Pijltje omhoog</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="266"/>
        <source>First Page</source>
        <translation>Eerste pagina</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="269"/>
        <source>Go to first page</source>
        <translation>Ga naar eerste pagina</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="272"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="277"/>
        <source>Last Page</source>
        <translation>Laatste pagina</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="280"/>
        <source>Go to last page</source>
        <translation>Ga naar laatste pagina</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="283"/>
        <source>End</source>
        <translation>End</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="292"/>
        <source>Back</source>
        <translation>Terug</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="295"/>
        <source>Back in navigation history</source>
        <translation>Terug in navigatiegeschiedenis</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="298"/>
        <source>Backspace</source>
        <translation>Backspace</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="307"/>
        <source>Forward</source>
        <translation>Vooruit</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="310"/>
        <source>Go to the next position in navigation history</source>
        <translation>Ga naar volgende locatie in navigatiegeschiedenis</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="313"/>
        <source>Shift+Backspace</source>
        <translation>Shift+Backspace</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="318"/>
        <source>Next Chapter</source>
        <translation>Volgend hoofdstuk</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="321"/>
        <source>Go to next chapter</source>
        <translation>Ga naar volgend hoofdstuk</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="324"/>
        <source>Alt+Down</source>
        <translation>Alt+pijltje omlaag</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="329"/>
        <source>Previous Chapter</source>
        <translation>Vorig hoofdstuk</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="332"/>
        <source>Go to previous chapter</source>
        <translation>Ga naar vorig hoofdstuk</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="335"/>
        <source>Alt+Up</source>
        <translation>Alt+pijltje omhoog</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="347"/>
        <source>Toggle Full Screen</source>
        <translation>Schermvullende modus aan/uit</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="350"/>
        <source>Toggle Full Screen mode</source>
        <translation>Schermvullende modus aan/uit</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="353"/>
        <source>Alt+Return</source>
        <translation>Alt+Enter</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="362"/>
        <source>Zoom In</source>
        <translation>Inzoomen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="365"/>
        <source>Increase font size</source>
        <translation>Lettertype vergroten</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="368"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="377"/>
        <source>Zoom Out</source>
        <translation>Uitzoomen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="380"/>
        <source>Decrease font size</source>
        <translation>Lettertype verkleinen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="383"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="392"/>
        <source>Table of Contents...</source>
        <translation>Inhoudsopgave…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="395"/>
        <source>Show table of contents</source>
        <translation>Toon de inhoudsopgave</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="398"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="407"/>
        <source>Recent Books</source>
        <translation>Onlangs geopend</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="410"/>
        <source>Show recent books list</source>
        <translation>Toon de onlangs geopende boeken</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="413"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="422"/>
        <source>Settings...</source>
        <translation>Instellingen…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="425"/>
        <source>Settings dialog</source>
        <translation>Open het instellingenvenster</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="428"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="437"/>
        <source>Copy</source>
        <translation>Kopiëren</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="440"/>
        <source>Copy selected text</source>
        <translation>Kopieer de selectie</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="443"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="448"/>
        <source>copy2</source>
        <translation>copy2</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="451"/>
        <source>Copy alternative shortcut</source>
        <translation>Alternatieve kopieersneltoets</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="454"/>
        <source>Ctrl+Ins</source>
        <translation>Ctrl+Ins</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="459"/>
        <source>About Qt...</source>
        <translation>Over Qt...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="462"/>
        <source>About Qt</source>
        <translation>Over Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="470"/>
        <source>About crqt-ng...</source>
        <translation>Over crqt-ng...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="473"/>
        <source>About the program</source>
        <translation>Over het programma</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="485"/>
        <source>Add Bookmark</source>
        <translation>Bladwijzer toevoegen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="488"/>
        <source>Add bookmark</source>
        <translation>Voeg een bladwijzer toe</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="491"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="500"/>
        <source>Bookmark List...</source>
        <translation>Alle bladwijzers…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="503"/>
        <source>Show bookmarks list</source>
        <translation>Toon een lijst met alle bladwijzers</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="506"/>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="515"/>
        <source>File Properties...</source>
        <translation>Bestandseigenschappen…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="518"/>
        <source>Show file properties</source>
        <translation>Toon de bestandseigenschappen</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="521"/>
        <source>Ctrl+F1</source>
        <translation>Ctrl+F1</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="530"/>
        <source>Rotate</source>
        <translation>Draaien</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="533"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="542"/>
        <source>Find text...</source>
        <translation>Zoeken…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="545"/>
        <source>Find text</source>
        <translation>Zoek naar tekst</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="548"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="553"/>
        <source>Toggle Edit Mode</source>
        <translation>Bewerkmodus aan/uit</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="556"/>
        <source>Toggle edit mode</source>
        <translation>Schakel de bewerkmodus in of uit</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="561"/>
        <source>Export</source>
        <translation>Exporteren</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="564"/>
        <source>Export document</source>
        <translation>Exporteer het huidige bestand</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="567"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="572"/>
        <location filename="../mainwindow.ui" line="575"/>
        <location filename="../mainwindow.ui" line="605"/>
        <location filename="../mainwindow.ui" line="608"/>
        <source>Next Page</source>
        <translation>Volgende pagina</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="578"/>
        <source>Right</source>
        <translation>Pijltje naar rechts</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="583"/>
        <location filename="../mainwindow.ui" line="586"/>
        <location filename="../mainwindow.ui" line="594"/>
        <location filename="../mainwindow.ui" line="597"/>
        <source>Previous Page</source>
        <translation>Vorige pagina</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="589"/>
        <source>Left</source>
        <translation>Pijltje naar links</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="600"/>
        <source>Shift+Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="611"/>
        <source>Space</source>
        <translation>Spatiebalk</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="616"/>
        <source>Next Sentence</source>
        <translation>Volgende zin</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="619"/>
        <source>Move selection to next sentence</source>
        <translation>Verplaats de selectie naar de volgende zin</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="622"/>
        <source>Ctrl+Right</source>
        <translation>Ctrl+pijltje naar rechts</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="627"/>
        <source>Prev Sentence</source>
        <translation>Vorige zin</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="630"/>
        <source>Select previous sentence</source>
        <translation>Verplaats de selectie naar de vorige zin</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="633"/>
        <source>Ctrl+Left</source>
        <translation>Ctrl+pijltje naar links</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="642"/>
        <source>New tab</source>
        <translation>Nieuw tabblad</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="645"/>
        <source>Open new tab</source>
        <translation>Open een nieuw tabblad</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="648"/>
        <source>Ctrl+Shift+T</source>
        <translation>Ctrl+Shift+T</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="657"/>
        <source>Open in new tab...</source>
        <translation>Openen op nieuw tabblad…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="666"/>
        <location filename="../mainwindow.ui" line="669"/>
        <source>Open link in new tab</source>
        <translation type="unfinished">Open link in nieuw tabblad</translation>
    </message>
</context>
<context>
    <name>RecentBooksDlg</name>
    <message>
        <location filename="../recentdlg.ui" line="14"/>
        <source>Recent Books</source>
        <translation>Onlangs geopend</translation>
    </message>
    <message>
        <location filename="../recentdlg.ui" line="41"/>
        <source>Remove Item</source>
        <translation>Item wissen</translation>
    </message>
    <message>
        <location filename="../recentdlg.ui" line="44"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../recentdlg.ui" line="49"/>
        <source>Clear All</source>
        <translation>Alles wissen</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="44"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="44"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="44"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="44"/>
        <source>Filename</source>
        <translation>Bestandsnaam</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="176"/>
        <source>Remove all history items</source>
        <translation>Alle geschiedenisitems wissen</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="177"/>
        <source>Do you really want to remove all history records?</source>
        <translation>Weet u zeker dat u alle geschiedenisitems wilt wissen?</translation>
    </message>
</context>
<context>
    <name>SampleView</name>
    <message>
        <location filename="../sampleview.cpp" line="30"/>
        <source>Style Preview</source>
        <translation>Voorvertoning</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../searchdlg.ui" line="14"/>
        <source>Search</source>
        <translation>Zoeken</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="25"/>
        <source>Text</source>
        <translation>Tekst</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="32"/>
        <source>Case Sensitive</source>
        <translation>Hoofdlettergevoelig</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="39"/>
        <source>Search forward</source>
        <translation>Vooruit zoeken</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="46"/>
        <source>Search backward</source>
        <translation>Achterwaarts zoeken</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="73"/>
        <source>Find Next</source>
        <translation>Volgende zoeken</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="83"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../searchdlg.cpp" line="144"/>
        <source>Not found</source>
        <translation>Niet gevonden</translation>
    </message>
    <message>
        <location filename="../searchdlg.cpp" line="144"/>
        <source>Search pattern is not found in document</source>
        <translation>De gezochte tekst is niet aangetroffen</translation>
    </message>
</context>
<context>
    <name>SettingsDlg</name>
    <message>
        <location filename="../settings.ui" line="23"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="37"/>
        <source>Window options</source>
        <translation>Vensterinstellingen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="43"/>
        <source>Window</source>
        <translation>Venster</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="51"/>
        <source>Look &amp; feel</source>
        <translation>Vormgeving:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="61"/>
        <source>Startup action</source>
        <translation>Na opstarten:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="69"/>
        <source>Restore session</source>
        <translation>Sessie herstellen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="74"/>
        <source>Show list of recent books</source>
        <translation>Onlangs geopende boeken tonen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="79"/>
        <source>Show File Open dialog</source>
        <translation>Bestandskiezer tonen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="84"/>
        <source>Do nothing</source>
        <translation>Niets doen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="92"/>
        <source>Controls</source>
        <translation>Vensteritems:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="101"/>
        <source>Show toolbar</source>
        <translation>Werkbalk tonen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="108"/>
        <source>Show menu</source>
        <translation>Menubalk tonen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="115"/>
        <source>Show scroll bar</source>
        <translation>Schuifbalk tonen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="122"/>
        <source>Show status bar</source>
        <translation>Statusbalk tonen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="129"/>
        <source>Fixed tab size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clipboard</source>
        <translation type="vanished">Klembord:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="145"/>
        <source>automatically copy text to clipboard when selected</source>
        <translation>Kopieer automatisch geselecteerde tekst</translation>
    </message>
    <message>
        <source>automatically copy when selected</source>
        <translation type="vanished">Selectie automatisch kopiëren</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="162"/>
        <source>Automatically send selected text to this application.
%TEXT% will be replaced with the selected text.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="169"/>
        <source>goldendict %TEXT%, qolibri %TEXT%, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="178"/>
        <source>Fullscreen display</source>
        <translation>Schermvullende weergave</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="202"/>
        <source>Page</source>
        <translation>Pagina</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="220"/>
        <source>One page</source>
        <translation>Eén pagina</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="225"/>
        <source>Two pages</source>
        <translation>Twee pagina&apos;s</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="230"/>
        <source>Scroll View</source>
        <translation>Verschuifmodus</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="244"/>
        <source>Vew Mode</source>
        <translation>Weergavemodus:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="257"/>
        <source>Page margins</source>
        <translation>Paginamarges:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="265"/>
        <location filename="../settings.ui" line="781"/>
        <location filename="../settings.ui" line="831"/>
        <location filename="../settings.ui" line="881"/>
        <location filename="../settings.ui" line="931"/>
        <location filename="../settings.cpp" line="591"/>
        <location filename="../settings.cpp" line="632"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="270"/>
        <location filename="../settings.ui" line="786"/>
        <location filename="../settings.ui" line="836"/>
        <location filename="../settings.ui" line="886"/>
        <location filename="../settings.ui" line="936"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="275"/>
        <location filename="../settings.ui" line="791"/>
        <location filename="../settings.ui" line="841"/>
        <location filename="../settings.ui" line="891"/>
        <location filename="../settings.ui" line="941"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="280"/>
        <location filename="../settings.ui" line="796"/>
        <location filename="../settings.ui" line="846"/>
        <location filename="../settings.ui" line="896"/>
        <location filename="../settings.ui" line="946"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="285"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="290"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="295"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="300"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="305"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="310"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="315"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="320"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="325"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="330"/>
        <source>15</source>
        <translation>15</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="335"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="340"/>
        <source>25</source>
        <translation>25</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="345"/>
        <source>30</source>
        <translation>30</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="350"/>
        <source>40</source>
        <translation>40</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="355"/>
        <source>50</source>
        <translation>50</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="360"/>
        <source>60</source>
        <translation>60</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="378"/>
        <source>Show page header</source>
        <translation>Paginakop tonen:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="386"/>
        <location filename="../settings.cpp" line="244"/>
        <location filename="../settings.cpp" line="251"/>
        <location filename="../settings.cpp" line="817"/>
        <source>None</source>
        <translation>Niet</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="391"/>
        <source>Page header</source>
        <translation>Paginakop</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="396"/>
        <source>Page footer</source>
        <translation>Voetnoten</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="412"/>
        <source>Book name</source>
        <translation>Titel van boek</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="419"/>
        <source>Page number</source>
        <translation>Paginanummer</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="426"/>
        <source>Page count</source>
        <translation>Aantal pagina&apos;s</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="433"/>
        <source>Position percent</source>
        <translation>Locatie (in procenten)</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="440"/>
        <source>Clock</source>
        <translation>Klok</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="447"/>
        <source>Battery status</source>
        <translation>Accustatus</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="480"/>
        <source>Header font</source>
        <translation>Koplettertype:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="497"/>
        <location filename="../settings.cpp" line="1099"/>
        <source>Page header text color</source>
        <translation>Tekstkleur van paginakop:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="512"/>
        <location filename="../settings.ui" line="578"/>
        <location filename="../settings.ui" line="655"/>
        <location filename="../settings.ui" line="706"/>
        <location filename="../settings.ui" line="996"/>
        <location filename="../settings.ui" line="1308"/>
        <source>Change</source>
        <translation>Wijzigen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="556"/>
        <source>Show footnotes at bottom of page</source>
        <translation>Voetnoten onderaan pagina tonen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="565"/>
        <location filename="../settings.cpp" line="1353"/>
        <source>Selection color</source>
        <translation>Selectiekleur:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="642"/>
        <source>Comment color</source>
        <translation>Opmerkingskleur:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="693"/>
        <source>Correction color</source>
        <translation>Correctiekleur:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="616"/>
        <source>Bookmark highlight</source>
        <translation>Bladwijzermarkening:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="624"/>
        <location filename="../settings.ui" line="762"/>
        <location filename="../settings.ui" line="812"/>
        <location filename="../settings.ui" line="862"/>
        <location filename="../settings.ui" line="912"/>
        <source>Disabled</source>
        <translation>Uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="629"/>
        <source>Solid fill</source>
        <translation>Vlakke opvulling</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="634"/>
        <location filename="../settings.cpp" line="818"/>
        <source>Underline</source>
        <translation>Onderstrepen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="754"/>
        <source>Inline images zoom in</source>
        <translation>Afbeeldingen inzoomen:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="767"/>
        <location filename="../settings.ui" line="817"/>
        <location filename="../settings.ui" line="867"/>
        <location filename="../settings.ui" line="917"/>
        <source>Integer scale</source>
        <translation>Inpassen (geheel)</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="772"/>
        <location filename="../settings.ui" line="822"/>
        <location filename="../settings.ui" line="872"/>
        <location filename="../settings.ui" line="922"/>
        <source>Arbitrary scale</source>
        <translation>Inpassen (arbitrair)</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="854"/>
        <source>Block images zoom in</source>
        <translation>Afbeeldingen niet inzoomen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="904"/>
        <source>Block images zoom out</source>
        <translation>Afbeeldingen niet uitzoomen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="804"/>
        <source>Inline images zoom out</source>
        <translation>Afbeeldingen uitzoomen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="138"/>
        <source>Automatically when selected:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="148"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="155"/>
        <source>Run command:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="406"/>
        <source>Header elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="744"/>
        <source>Image scaling options</source>
        <translation>Inpasopties</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="970"/>
        <source>Styles</source>
        <translation>Stijlen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="981"/>
        <location filename="../settings.cpp" line="1091"/>
        <source>Text color</source>
        <translation>Tekstkleur</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1054"/>
        <source>Default font</source>
        <translation>Standaardlettertype</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1119"/>
        <location filename="../settings.ui" line="1662"/>
        <source>Font weight</source>
        <translation>Tekstdikte</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1136"/>
        <source>Font gamma</source>
        <translation>Tekstgamma</translation>
    </message>
    <message>
        <source>0.3</source>
        <translation type="vanished">0.3</translation>
    </message>
    <message>
        <source>0.35</source>
        <translation type="vanished">0,35</translation>
    </message>
    <message>
        <source>0.4</source>
        <translation type="vanished">0,4</translation>
    </message>
    <message>
        <source>0.45</source>
        <translation type="vanished">0,45</translation>
    </message>
    <message>
        <source>0.5</source>
        <translation type="vanished">0,5</translation>
    </message>
    <message>
        <source>0.55</source>
        <translation type="vanished">0,55</translation>
    </message>
    <message>
        <source>0.6</source>
        <translation type="vanished">0,6</translation>
    </message>
    <message>
        <source>0.65</source>
        <translation type="vanished">0,65</translation>
    </message>
    <message>
        <source>0.7</source>
        <translation type="vanished">0,7</translation>
    </message>
    <message>
        <source>0.75</source>
        <translation type="vanished">0,75</translation>
    </message>
    <message>
        <source>0.8</source>
        <translation type="vanished">0,8</translation>
    </message>
    <message>
        <source>0.85</source>
        <translation type="vanished">0,85</translation>
    </message>
    <message>
        <source>0.9</source>
        <translation type="vanished">0,9</translation>
    </message>
    <message>
        <source>0.95</source>
        <translation type="vanished">0,95</translation>
    </message>
    <message>
        <source>0.98</source>
        <translation type="vanished">0,98</translation>
    </message>
    <message>
        <source>1.0</source>
        <translation type="vanished">1,0</translation>
    </message>
    <message>
        <source>1.02</source>
        <translation type="vanished">1,02</translation>
    </message>
    <message>
        <source>1.05</source>
        <translation type="vanished">1,05</translation>
    </message>
    <message>
        <source>1.1</source>
        <translation type="vanished">1,1</translation>
    </message>
    <message>
        <source>1.15</source>
        <translation type="vanished">1,15</translation>
    </message>
    <message>
        <source>1.2</source>
        <translation type="vanished">1,2</translation>
    </message>
    <message>
        <source>1.25</source>
        <translation type="vanished">1,25</translation>
    </message>
    <message>
        <source>1.3</source>
        <translation type="vanished">1,3</translation>
    </message>
    <message>
        <source>1.35</source>
        <translation type="vanished">1,35</translation>
    </message>
    <message>
        <source>1.4</source>
        <translation type="vanished">1,4</translation>
    </message>
    <message>
        <source>1.45</source>
        <translation type="vanished">1,45</translation>
    </message>
    <message>
        <source>1.5</source>
        <translation type="vanished">1,5</translation>
    </message>
    <message>
        <source>1.6</source>
        <translation type="vanished">1,6</translation>
    </message>
    <message>
        <source>1.7</source>
        <translation type="vanished">1,7</translation>
    </message>
    <message>
        <source>1.8</source>
        <translation type="vanished">1,8</translation>
    </message>
    <message>
        <source>1.9</source>
        <translation type="vanished">1,9</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1174"/>
        <source>Font antialiasing</source>
        <translation>Anti-kartelvorming</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1188"/>
        <source>Font hinting</source>
        <translation>Hinten</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1220"/>
        <source>No hinting</source>
        <translation>Niet hinten</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1225"/>
        <source>Use bytecode</source>
        <translation>Bytecode-hinten</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1230"/>
        <source>Autohinting</source>
        <translation>Automatisch hinten</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1238"/>
        <source>Fallback fonts</source>
        <translation>Alternatieve lettertypen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1251"/>
        <location filename="../settings.ui" line="1562"/>
        <source>Manage...</source>
        <translation>Beheren…</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1258"/>
        <source>Page skin</source>
        <translation>Paginathema</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1293"/>
        <location filename="../settings.cpp" line="1095"/>
        <source>Background color</source>
        <translation>Achtergrondkleur</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1354"/>
        <source>Rendering flags</source>
        <translation>Laadopties</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1362"/>
        <location filename="../settings.ui" line="1393"/>
        <source>Legacy</source>
        <translation>Verouderd</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1367"/>
        <source>Flat</source>
        <translation>Plat</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1372"/>
        <source>Book</source>
        <translation>Boek</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1377"/>
        <source>Web (Full)</source>
        <translation>Web (volledig)</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1385"/>
        <source>DOM level:</source>
        <translation>DOM-niveau:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1398"/>
        <source>Newest</source>
        <translation>Nieuwste</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1406"/>
        <source>Multi languages</source>
        <translation>Meerdere talen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1413"/>
        <source>Support for multilingual documents</source>
        <translation>Bestanden met meerdere talen toestaan</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1420"/>
        <location filename="../settings.ui" line="1434"/>
        <source>Hyphenation</source>
        <translation>Woordafbreking</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1427"/>
        <source>Enable hyphenation</source>
        <translation>Woorden afbreken</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1444"/>
        <source>Text shaping</source>
        <translation>Tekstvorming</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1458"/>
        <source>Interline spacing</source>
        <translation>Interlinie-afstand</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1468"/>
        <source>Min space width</source>
        <translation>Min. spatiebreedte</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1478"/>
        <source>Font kerning</source>
        <translation>Lettertypekernering</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1485"/>
        <source>Enable font kerning</source>
        <translation>Lettertypekernering inschakelen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1492"/>
        <source>Floating punctuation</source>
        <translation>Zwevende leestekens</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1499"/>
        <source>Enable floating punctuation</source>
        <translation>Zwevende leestekens inschakelen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1506"/>
        <source>Internal CSS</source>
        <translation>Interne css</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1513"/>
        <source>Enable document internal styles</source>
        <translation>Interne bestandsstijlen gebruiken</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1520"/>
        <source>.EPUB</source>
        <translation>.epub-bestanden</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1527"/>
        <source>Enable document embedded fonts</source>
        <translation>Meegeleverde lettertypen gebruiken</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1534"/>
        <source>Margins redefine</source>
        <translation>Marges</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1541"/>
        <source>Ignore document margins</source>
        <translation>Bestandsmarges negeren</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1548"/>
        <source>.TXT files</source>
        <translation>.txt-bestanden</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1555"/>
        <source>Disable automatic formatting</source>
        <translation>Automatische opmaak uitschakelen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1569"/>
        <source>Font families</source>
        <translation>Lettertypen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1579"/>
        <source>Stylesheet</source>
        <translation>Stijlblad</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1588"/>
        <location filename="../settings.cpp" line="456"/>
        <source>Default paragraph style</source>
        <translation>Standaard alineastijl</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1622"/>
        <source>Alignment</source>
        <translation>Uitlijning</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1632"/>
        <source>First line</source>
        <translation>Eerste regel</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1642"/>
        <source>Font size</source>
        <translation>Tekstgrootte</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1652"/>
        <source>Font face</source>
        <translation>Lettertype</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1672"/>
        <source>Font style</source>
        <translation>Tekststijl</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1682"/>
        <source>Font color</source>
        <translation>Tekstkleur</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1722"/>
        <source>Margins:</source>
        <translation>Marges:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1729"/>
        <source>Before</source>
        <translation>Vóór</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1739"/>
        <source>After</source>
        <translation>Na</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1749"/>
        <location filename="../settings.cpp" line="545"/>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1759"/>
        <location filename="../settings.cpp" line="545"/>
        <source>Right</source>
        <translation>Rechts</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1695"/>
        <source>Interline space</source>
        <translation>Interlinie-afstand</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1705"/>
        <source>Text decoration</source>
        <translation>Tekstdecoratie</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1715"/>
        <source>Vertical align</source>
        <translation>Verticale uitlijning</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="216"/>
        <source>Simple (FreeType only, fastest)</source>
        <translation>Eenvoudig (FreeType - snelst)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="217"/>
        <source>Light (HarfBuzz without ligatures)</source>
        <translation>Licht (HarfBuzz - zonder ligaturen)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="218"/>
        <source>Full (HarfBuzz with ligatures)</source>
        <translation>Volledig (HarfBuzz - met ligaturen)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="245"/>
        <location filename="../settings.cpp" line="252"/>
        <location filename="../settings.cpp" line="756"/>
        <source>Gray</source>
        <translation>Grijs</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="246"/>
        <source>LCD (RGB)</source>
        <translation>Lcd (rgb)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="247"/>
        <source>LCD (BGR)</source>
        <translation>Lcd (bgr)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="248"/>
        <source>LCD (RGB) vertical</source>
        <translation>Lcd (rgb) verticaal</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="249"/>
        <source>LCD (BGR) vertical</source>
        <translation>Lcd (bgr) verticaal</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="429"/>
        <source>[No hyphenation]</source>
        <translation>[Niet afbreken]</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="431"/>
        <source>[Algorythmic hyphenation]</source>
        <translation>[Algoritmisch afbreken]</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="457"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="458"/>
        <source>Subtitle</source>
        <translation>Ondertitel</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="459"/>
        <source>Preformatted text</source>
        <translation>Vooraf opgemaakte tekst</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="460"/>
        <source>Link</source>
        <translation>Link</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="461"/>
        <source>Cite / quotation</source>
        <translation>Citaat</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="462"/>
        <source>Epigraph</source>
        <translation>Voorwoord</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="463"/>
        <source>Poem</source>
        <translation>Gedicht</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="464"/>
        <source>Text author</source>
        <translation>Tekstauteur</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="465"/>
        <source>Footnote link</source>
        <translation>Voetnootlink</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="466"/>
        <source>Footnote</source>
        <translation>Voetnoot</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="467"/>
        <source>Footnote title</source>
        <translation>Voetnoottitel</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="468"/>
        <source>Annotation</source>
        <translation>Aantekening</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="545"/>
        <location filename="../settings.cpp" line="561"/>
        <location filename="../settings.cpp" line="590"/>
        <location filename="../settings.cpp" line="631"/>
        <location filename="../settings.cpp" line="664"/>
        <location filename="../settings.cpp" line="686"/>
        <location filename="../settings.cpp" line="705"/>
        <location filename="../settings.cpp" line="751"/>
        <location filename="../settings.cpp" line="816"/>
        <location filename="../settings.cpp" line="834"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="545"/>
        <source>Justify</source>
        <translation>Uitvullen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="545"/>
        <source>Center</source>
        <translation>Centreren</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="561"/>
        <source>No indent</source>
        <translation>Niet inspringen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="561"/>
        <source>Small Indent</source>
        <translation>Licht inspringen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="561"/>
        <source>Big Indent</source>
        <translation>Meer inspringen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="561"/>
        <source>Small Outdent</source>
        <translation>Licht uitspringen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="561"/>
        <source>Big Outdent</source>
        <translation>Meer uitspringen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="592"/>
        <source>20% of line height</source>
        <translation>20% regelhoogte</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="593"/>
        <source>30% of line height</source>
        <translation>30% regelhoogte</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="594"/>
        <location filename="../settings.cpp" line="633"/>
        <source>50% of line height</source>
        <translation>50% regelhoogte</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="595"/>
        <location filename="../settings.cpp" line="634"/>
        <source>100% of line height</source>
        <translation>100% regelhoogte</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="596"/>
        <location filename="../settings.cpp" line="635"/>
        <source>150% of line height</source>
        <translation>150% regelhoogte</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="636"/>
        <source>200% of line height</source>
        <translation>200% regelhoogte</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="637"/>
        <source>400% of line height</source>
        <translation>400% regelhoogte</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="638"/>
        <source>5% of line width</source>
        <translation>5% regelbreedte</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="639"/>
        <source>10% of line width</source>
        <translation>10% regelbreedte</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="640"/>
        <source>15% of line width</source>
        <translation>15% regelbreedte</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="641"/>
        <source>20% of line width</source>
        <translation>20% regelbreedte</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="642"/>
        <source>30% of line width</source>
        <translation>30% regelbreedte</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="665"/>
        <location filename="../settings.cpp" line="706"/>
        <source>Normal</source>
        <translation>Normaal</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="666"/>
        <source>Bold</source>
        <translation>Vetgedrukt</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="667"/>
        <source>Bolder</source>
        <translation>Vetgedrukter</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="668"/>
        <source>Lighter</source>
        <translation>Lichter</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="687"/>
        <source>Increase: 110%</source>
        <translation>Vergroten: 110%</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="688"/>
        <source>Increase: 120%</source>
        <translation>Vergroten: 120%</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="689"/>
        <source>Increase: 150%</source>
        <translation>Vergroten: 150%</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="690"/>
        <source>Decrease: 90%</source>
        <translation>Vergroten: 90%</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="691"/>
        <source>Decrease: 80%</source>
        <translation>Vergroten: 80%</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="692"/>
        <source>Decrease: 70%</source>
        <translation>Vergroten: 70%</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="693"/>
        <source>Decrease: 60%</source>
        <translation>Vergroten: 60%</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="707"/>
        <source>Italic</source>
        <translation>Cursief</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="716"/>
        <source>[Default Sans Serif]</source>
        <translation>[Standaard schreefloos]</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="718"/>
        <source>[Default Serif]</source>
        <translation>[Standaard met schreef]</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="720"/>
        <source>[Default Monospace]</source>
        <translation>[Standaard vaste breedte]</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="752"/>
        <source>Black</source>
        <translation>Zwart</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="753"/>
        <source>Green</source>
        <translation>Groen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="754"/>
        <source>Silver</source>
        <translation>Zilver</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="755"/>
        <source>Lime</source>
        <translation>Limoen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="757"/>
        <source>Olive</source>
        <translation>Olijfgroen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="758"/>
        <source>White</source>
        <translation>Wit</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="759"/>
        <source>Yellow</source>
        <translation>Geel</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="760"/>
        <source>Maroon</source>
        <translation>Kastanjebruin</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="761"/>
        <source>Navy</source>
        <translation>Marineblauw</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="762"/>
        <source>Red</source>
        <translation>Rood</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="763"/>
        <source>Blue</source>
        <translation>Blauw</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="764"/>
        <source>Purple</source>
        <translation>Paars</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="765"/>
        <source>Teal</source>
        <translation>Groenblauw</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="766"/>
        <source>Fuchsia</source>
        <translation>Fuchsia</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="767"/>
        <source>Aqua</source>
        <translation>Aquamarijn</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="819"/>
        <source>Line through</source>
        <translation>Doorhalen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="820"/>
        <source>Overline</source>
        <translation>Bovenstrepen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="835"/>
        <source>Baseline</source>
        <translation>Basis</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="836"/>
        <source>Subscript</source>
        <translation>Subscript</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="837"/>
        <source>Superscript</source>
        <translation>Superscript</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1033"/>
        <source>The quick brown fox jumps over the lazy dog. </source>
        <translation>Filmquiz bracht knappe ex-yogi van de wijs. </translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1202"/>
        <source>synthetic*</source>
        <translation>synthetisch*</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1204"/>
        <source>synthetic</source>
        <translation>synthetisch</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1357"/>
        <source>Comment bookmark color</source>
        <translation>Bladwijzerkleur van opmerkingen</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1361"/>
        <source>Correction bookmark color</source>
        <translation>Bladwijzerkleur van correcties</translation>
    </message>
</context>
<context>
    <name>TocDlg</name>
    <message>
        <location filename="../tocdlg.ui" line="14"/>
        <source>Table of Contents</source>
        <translation>Inhoudsopgave</translation>
    </message>
    <message>
        <location filename="../tocdlg.cpp" line="75"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../tocdlg.cpp" line="75"/>
        <source>Page</source>
        <translation>Pagina</translation>
    </message>
</context>
<context>
    <name>WolExportDlg</name>
    <message>
        <location filename="../wolexportdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoogvenster</translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="22"/>
        <source>Bits per pixel</source>
        <translation>Aantal bits per pixel</translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="30"/>
        <location filename="../wolexportdlg.ui" line="51"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="35"/>
        <location filename="../wolexportdlg.ui" line="56"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="43"/>
        <source>Table of Contents levels</source>
        <translation>Aantal inhoudsopgaveniveaus</translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="61"/>
        <source>3</source>
        <translation>3</translation>
    </message>
</context>
<context>
    <name>crqtutils</name>
    <message>
        <location filename="../crqtutil.cpp" line="184"/>
        <source>Undetermined</source>
        <translation>Onbepaald</translation>
    </message>
</context>
</TS>
