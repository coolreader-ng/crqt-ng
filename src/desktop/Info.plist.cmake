<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist SYSTEM "file://localhost/System/Library/DTDs/PropertyList.dtd">
<plist version="0.1">
<dict>
	<key>CFBundleName</key>
	<string>crqt-ng</string>
	<key>CFBundleDisplayName</key>
	<string>crqt-ng</string>
	<key>CFBundleVersion</key>
	<string>@VERSION@</string>
	<key>CFBundleShortVersionString</key>
	<string>@VERSION@</string>
	<key>CFBundleIconFile</key>
	<string>crqt.icns</string>
	<key>CFBundleIdentifier</key>
	<string>io.gitlab.coolreader-ng.crqt</string>
	<key>CFBundlePackageType</key>
	<string>APPL</string>
	<key>CFBundleSignature</key>
	<string>crdr</string>
	<key>CFBundleExecutable</key>
	<string>crqt</string>
	<key>NSPrincipalClass</key>
	<string>NSApplication</string>
	<key>NSHighResolutionCapable</key>
	<string>True</string>
	<key>NSHumanReadableCopyright</key>
	<string>(c) 2023,2024 coolreader-ng</string>
</dict>
</plist>
